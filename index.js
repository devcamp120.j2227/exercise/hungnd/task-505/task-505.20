// Import thư viện Express Js
const express = require("express");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

let arr = ["Việt Nam", "Mỹ", "Ấn Độ"];

// Khai báo GET API trả ra simple message
app.get("/", (request, response) => {
    let today = new Date();
    let str = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;
    response.json({
        message: str
    })
});

// khai báo phương thức GET
app.get("/get-method", (req, res) => {
    res.json({
        arr: arr
    })
});

// Khai báo phương thức POST
app.post("/post-method", (req, res) => {
    res.json({
        arr: [...arr, "Canada"]
    })
});

// Khai báo phương thức PUT
app.put("/put-method", (req, res) => {
    arr[3] = "Brazil";
    res.json({
        arr: arr
    })
});

// Khai báo phương thức DELETE
app.delete("/delete-method", (req, res) => {
    let [first, ...slice_arr] = arr
    res.json({
        arr: slice_arr
    })
});
// Chạy app trên cổng đã khai báo
app.listen(port, () => {
    console.log(`App đã chạy trên cổng ${port}`);
});
